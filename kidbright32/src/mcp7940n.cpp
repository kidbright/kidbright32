#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "kidbright32.h"
#include "mcp7940n.h"

#define MCP7940N_WRITE_FLAG					0x01
#define MCP7940N_CAL_FLAG					0x02
#define MCP7940N_CAL_COARSE_FLAG			0x04

MCP7940N::MCP7940N(int bus_ch, int dev_addr) {
	channel = bus_ch;
	address = dev_addr;
	polling_ms = MCP7940N_POLLING_MS;
}

void MCP7940N::init(void) {
	flag = 0;
	datetime_str[0] = '\x0';
	date_str[0] = '\x0';
	time_str[0] = '\x0';;
	state = s_detect;
}

int MCP7940N::prop_count(void) {
	// not supported
	return 0;
}

bool MCP7940N::prop_name(int index, char *name) {
	// not supported
	return false;
}

bool MCP7940N::prop_unit(int index, char *unit) {
	// not supported
	return false;
}

bool MCP7940N::prop_attr(int index, char *attr) {
	// not supported
	return false;
}

bool MCP7940N::prop_read(int index, char *value) {
	// not supported
	return false;
}

bool MCP7940N::prop_write(int index, char *value) {
	// not supported
	return false;
}

void MCP7940N::process(Driver *drv) {
	I2CDev *i2c = (I2CDev *)drv;
	uint8_t data[MCP7940N_BUFFER_SIZE], pointer;

	switch (state) {
		case s_detect:
			// stamp polling tickcnt
			polling_tickcnt = get_tickcnt();
			// detect i2c device
			if (i2c->detect(channel, address) == ESP_OK) {
				// clear error flag
				error = false;
				state = s_read;
			}
			else {
				state = s_error;
			}
			break;

		case s_read:
			pointer = 0;
			if (i2c->read(channel, address, &pointer, 1, data, 7) == ESP_OK) {
				// check if osc stopped
				if ((data[0] & 0x80) == 0) {
					// write default date/time
					write((char *)MCP7940N_DEFAULT_DATETIME); // 0 = sunday
				}
				else {
					data[0] &= 0x7f;	// clear unused bit
					data[2] &= 0x3f;	// clear hour am/pm bit
					data[3] &= 0x07;	// clear unused bit
					data[5] &= 0x1f;	// clear month century and unused bit
					memcpy(read_buffer, data, 9);
					// set initialized flag
					initialized = true;
				}
				// load polling tickcnt
				tickcnt = polling_tickcnt;
				state = s_wait;
			}
			else {
				state = s_error;
			}

			if (state != s_wait) {
				state = s_error;
			}
			break;

		case s_wait:
			if (error) {
				// wait polling_ms timeout
				if (is_tickcnt_elapsed(tickcnt, polling_ms)) {
					state = s_detect;
				}
			}
			else {
				// check if cal flag set
				if (IS_FLAG_SET(flag, MCP7940N_CAL_FLAG)) {
					FLAG_CLR(flag, MCP7940N_CAL_FLAG);
					state = s_cal;
				}
				else
				// check if cal coarse flag set
				if (IS_FLAG_SET(flag, MCP7940N_CAL_COARSE_FLAG)) {
					FLAG_CLR(flag, MCP7940N_CAL_COARSE_FLAG);
					state = s_cal_coarse;
				}
				else
				// check if write flag set
				if (IS_FLAG_SET(flag, MCP7940N_WRITE_FLAG)) {
					state = s_write;
				}
				else {
					// wait polling_ms timeout
					if (is_tickcnt_elapsed(tickcnt, polling_ms)) {
						state = s_detect;
					}
				}
			}
			break;

		case s_write:
			data[0] = 0; // address pointer
			memcpy(&data[1], write_buffer, 9);
			if (i2c->write(channel, address, data, 10) == ESP_OK) {
				// finally, clear rtc write flag
				FLAG_CLR(flag, MCP7940N_WRITE_FLAG);
				state = s_detect;
			}
			else {
				state = s_error;
			}
			break;

		case s_cal:
			/*i2c_master_start();
			i2c_master_writeByte(I2C_ADDR_WRITE(ds3231_addr));
			if (i2c_master_checkAck()) {
				// set address pointer to 7
				i2c_master_writeByte(7);
				if (i2c_master_checkAck()) {
					// [CONTROL, address 0x07, default=0x80]
					// OUT SQWEN=1 ALM1EN ALM0EN EXTOSC CRSTRIM SQWFS1 SQWFS0
					// [SQWFS1 SQWFS0]
					// 00 = 1 Hz
					// 01 = 4.096 kHz
					// 10 = 8.192 kHz
					// 11 = 32.768 kHz
					i2c_master_writeByte(0x41); // 4.096 kHz
					if (i2c_master_checkAck()) {
						i2c_master_writeByte(cal_value);
						if (i2c_master_checkAck()) {
							os_printf("mcp7940n digital trimming done.\n");
						}
					}
				}
			}
			i2c_master_stop();
			state = s_detect;*/
			break;

		case s_cal_coarse:
			/*i2c_master_start();
			i2c_master_writeByte(I2C_ADDR_WRITE(ds3231_addr));
			if (i2c_master_checkAck()) {
				// set address pointer to 7
				i2c_master_writeByte(7);
				if (i2c_master_checkAck()) {
					// [CONTROL, address 0x07, default=0x80]
					// OUT SQWEN=1 ALM1EN ALM0EN EXTOSC CRSTRIM SQWFS1 SQWFS0
					// [SQWFS1 SQWFS0]
					// 00 = 1 Hz
					// 01 = 4.096 kHz
					// 10 = 8.192 kHz
					// 11 = 32.768 kHz
					i2c_master_writeByte(0x45);
					if (i2c_master_checkAck()) {
						i2c_master_writeByte(cal_coarse_value);
						if (i2c_master_checkAck()) {
							os_printf("mcp7940n digital trimming (coarse mode) done.\n");
						}
					}
				}
			}
			i2c_master_stop();
			state = s_detect;*/
			break;

		case s_error:
			// set error flag
			error = true;
			// clear initialized flag
			initialized = false;
			// get current tickcnt
			tickcnt = get_tickcnt();
			// goto wait and retry with detect state
			state = s_wait;
			break;
	}
}

char *MCP7940N::get_datetime(void) {
	uint8_t tmp[MCP7940N_BUFFER_SIZE];

	memcpy(tmp, read_buffer, MCP7940N_BUFFER_SIZE);
	snprintf(&datetime_str[0], DATETIME_STR_LEN, "%02x/%02x/20%02x %02x:%02x", tmp[4], tmp[5], tmp[6], tmp[2], tmp[1]);

	return &datetime_str[0];
}

char *MCP7940N::get_datetime_with_second(void) {
	uint8_t tmp[MCP7940N_BUFFER_SIZE];

	memcpy(tmp, read_buffer, MCP7940N_BUFFER_SIZE);
	snprintf(&datetime_str[0], DATETIME_STR_LEN, "%02x/%02x/20%02x %02x:%02x:%02x", tmp[4], tmp[5], tmp[6], tmp[2], tmp[1], tmp[0]);

	return &datetime_str[0];
}

char *MCP7940N::get_date(void) {
	uint8_t tmp[MCP7940N_BUFFER_SIZE];

	memcpy(tmp, read_buffer, MCP7940N_BUFFER_SIZE);
	snprintf(&date_str[0], DATE_STR_LEN, "%02x/%02x/20%02x", tmp[4], tmp[5], tmp[6]);

	return &date_str[0];
}

char *MCP7940N::get_time(void) {
	uint8_t tmp[MCP7940N_BUFFER_SIZE];

	memcpy(tmp, read_buffer, MCP7940N_BUFFER_SIZE);
	snprintf(&time_str[0], TIME_STR_LEN, "%02x:%02x", tmp[2], tmp[1]);

	return &time_str[0];
}

char *MCP7940N::get_time_with_second(void) {
	uint8_t tmp[MCP7940N_BUFFER_SIZE];

	memcpy(tmp, read_buffer, MCP7940N_BUFFER_SIZE);
	snprintf(&time_str[0], TIME_STR_LEN, "%02x:%02x:%02x", tmp[2], tmp[1], tmp[0]);

	return &time_str[0];
}

int bcd2int(int bcd) {
	return (bcd >> 4) * 10 + (bcd & 0x0f);
}

uint8_t str2dec(char *str) {
	return ((str[0] - 0x30) * 10) + (str[1] - 0x30);
}

uint8_t str2bcd(char *str) {
	return (((str[0] - 0x30) << 4) & 0xf0) | ((str[1] - 0x30) & 0x0f);
}

int MCP7940N::get(int index) {
	uint8_t tmp[MCP7940N_BUFFER_SIZE];

	memcpy(tmp, read_buffer, MCP7940N_BUFFER_SIZE);
	// 0..5 = day,month,year,hour,minute,second
	if ((index >= 0) && (index <= 1)) {
		return bcd2int(tmp[4 + index]);
	}
	else
	if (index == 2) {
		return (2000 + bcd2int(tmp[4 + index]));
	}
	else
	if ((index >= 3) && (index <= 5)) {
		return bcd2int(tmp[5 - index]);
	}
	else {
		return 0;
	}
}

void MCP7940N::write(char *str) {
	int i;

	// str format: moment().format('YYMMDD0eHHmmss');

	// second, minute, hour
	for (i=0; i<3; i++) {
		write_buffer[2 - i] = str2bcd(&str[8 + i*2]);
	}
	// day of week, ds3231 = one based
	write_buffer[3] = str2dec(&str[6]) + 1;
	// day, month, year
	for (i=0; i<3; i++) {
		write_buffer[6 - i] = str2bcd(&str[i * 2]);
	}

	// set MCP7940 ST bit
	write_buffer[0] |= 0x80;
	// set MCP7940 VBATEN bit
	write_buffer[3] |= 0x08;

	// [CONTROL, address 0x07, default=0x80]
	// OUT SQWEN ALM1EN ALM0EN EXTOSC CRSTRIM SQWFS1 SQWFS0
	write_buffer[7] = 0x80;
	// [OSCTRIM, address 0x08, default=0x00]
	// SIGN TRIMVAL6 TRIMVAL5 TRIMVAL4 TRIMVAL3 TRIMVAL2 TRIMVAL1 TRIMVAL0
	write_buffer[8] = 0x00;

	FLAG_SET(flag, MCP7940N_WRITE_FLAG);
}

bool MCP7940N::write_flag(void) {
	return IS_FLAG_SET(flag, MCP7940N_WRITE_FLAG);
}

void MCP7940N::cal(int val) {
	if ((val >= -127) && (val <= 127)) {
		if (val < 0) {
			cal_value = (-1 * val);
		}
		else {
			cal_value = val | 0x80;
		}
		FLAG_SET(flag, MCP7940N_CAL_FLAG);
	}
}

void MCP7940N::cal_coarse(int val) {
	if ((val >= -127) && (val <= 127)) {
		if (val < 0) {
			cal_coarse_value = (-1 * val);
		}
		else {
			cal_coarse_value = val | 0x80;
		}
		FLAG_SET(flag, MCP7940N_CAL_COARSE_FLAG);
	}
}
