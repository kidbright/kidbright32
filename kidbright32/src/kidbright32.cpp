#include <stdio.h>
#include <string.h>
#include "driver/uart.h"
#include "soc/uart_struct.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "kidbright32.h"
#include "devman.h"

#define UART_NUM				UART_NUM_0
#define UART_BAUDRATE			115200
#define TX_BUF_SIZE				1024
#define RX_BUF_SIZE				1024

void vUserAppTask(void *pvParameters);

void StrSplit(char *cmd, char list[][MAX_UART_LINE_CHAR_COUNT], int *cnt, int max_split_cnt) {
	char *ptr;

	if (*cnt < (max_split_cnt - 1)) {
		ptr = strstr(cmd, " ");
		if (ptr) {
			*ptr = '\x0';
			strcpy(list[*cnt], cmd);
			ptr++;
			(*cnt)++;
			StrSplit(ptr, list, cnt, max_split_cnt);
		}
		else {
			strcpy(list[*cnt], cmd);
			(*cnt)++;
		}
	}
	else {
		strcpy(list[*cnt], cmd);
		(*cnt)++;
	}
}

void vUartTask(void *pvParameters) {
	char sbuf[64];
	char buf[128], temp_buf[128];
	int len, index;
	char list[8][MAX_UART_LINE_CHAR_COUNT];
	int cnt;

	index = 0;
	buf[0] = '\x0';
	while (1) {
		len = uart_read_bytes(UART_NUM, (uint8_t *)sbuf, sizeof(sbuf) - 1, (20 / portTICK_RATE_MS));
		if (len > 0) {
			sbuf[len] = '\x0';
			if ((len + index) < (sizeof(buf) - 1)) {
				strcat(&buf[index], sbuf);
				index += len;
				len = strlen(buf);
				// check cli command line terminated
				if ((buf[len - 1] == '\r') || (buf[len - 1] == '\n')) {
					buf[len - 1] = '\x0';
					strcpy(temp_buf, buf);
					// uart parser
					cnt = 0;
					StrSplit(buf, list, &cnt, MAX_UART_SPLIT_COUNT);

					if (cnt > 0) {
						// devman cli
						if (!devman_cli(temp_buf, list, cnt)) {
							// not handled
							printf("error\r\n");
						}
					}

					index = 0;
					buf[0] = '\x0';
				}
			}
			else {
				index = 0;
				buf[0] = '\x0';
			}
		}
	}

	vTaskDelete(NULL);
}

extern "C" void app_main() {
	nvs_flash_init();

	// uart init
	uart_config_t uart_config = {
		.baud_rate = UART_BAUDRATE,
		.data_bits = UART_DATA_8_BITS,
		.parity = UART_PARITY_DISABLE,
		.stop_bits = UART_STOP_BITS_1,
		.flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
		.rx_flow_ctrl_thresh = 0,
		.use_ref_tick = true
	};

	// uart config
	uart_param_config(UART_NUM, &uart_config);
	// set uart pins
	uart_set_pin(UART_NUM, 1, 3, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
	// install uart driver
	uart_driver_install(UART_NUM, (RX_BUF_SIZE * 2), (TX_BUF_SIZE * 2), 0, NULL, 0);
	// uart task
	xTaskCreatePinnedToCore(vUartTask, "Uart Task", UART_STACK_SIZE_MIN, NULL, UART_TASK_PRIORITY, NULL, KIDBRIGHT_RUNNING_CORE);

	// leds init
	gpio_config_t io_conf;
	io_conf.intr_type = GPIO_INTR_DISABLE; // disable interrupt
	io_conf.mode = GPIO_MODE_OUTPUT; // set as output mode
	io_conf.pin_bit_mask = (1ULL << BT_LED_GPIO) | (1ULL << WIFI_LED_GPIO) | (1ULL << NTP_LED_GPIO) | (1ULL << IOT_LED_GPIO); // pin bit mask
	io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE; // disable pull-down mode
	io_conf.pull_up_en = GPIO_PULLUP_DISABLE; // disable pull-up mode
	gpio_set_level(BT_LED_GPIO, LED_OFF);
	gpio_set_level(WIFI_LED_GPIO, LED_OFF);
	gpio_set_level(NTP_LED_GPIO, LED_OFF);
	gpio_set_level(IOT_LED_GPIO, LED_OFF);
	gpio_config(&io_conf);

	// log esp idf version
	ESP_LOGI("kidbright32", "KidBright32 (IDF %s)", esp_get_idf_version());

	// disable all system logs
	esp_log_level_set("*", ESP_LOG_NONE);

	// delay some
	vTaskDelay(100 / portTICK_RATE_MS);
	printf("\r\n");

	// user app task
	xTaskCreatePinnedToCore(vUserAppTask, "User App Task", USERAPP_STACK_SIZE_MIN, NULL, USERAPP_TASK_PRIORITY, NULL, KIDBRIGHT_RUNNING_CORE);
}
