#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "i2c-dev.h"
#include "kidbright32.h"

// https://esp-idf.readthedocs.io/en/latest/api-reference/peripherals/i2c.html

I2CDev::I2CDev(i2c_port_t _i2c_num, gpio_num_t _sda, gpio_num_t _scl, uint32_t _frequency) {
	i2c_num = _i2c_num;
	sda = _sda;
	scl = _scl;
	frequency = _frequency;
}

esp_err_t I2CDev::init(void) {
	esp_err_t ret;
	i2c_config_t conf;

    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = sda;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = scl;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = frequency;

	// ESP_OK Success
	// ESP_ERR_INVALID_ARG Parameter error
    if ((ret = i2c_param_config(i2c_num, &conf)) != ESP_OK) {
		return ret;
	}

	// ESP_OK					Success
	// ESP_ERR_INVALID_ARG		Parameter error
	// ESP_FAIL					Driver install error
    return i2c_driver_install(i2c_num, conf.mode, 0, 0, 0);
}

esp_err_t I2CDev::ch_sw(int ch, bool on_off) {
	i2c_cmd_handle_t cmd;
	uint8_t ch_addr;
	esp_err_t ret;
	uint8_t data;

	if ((ch >= 1) && (ch <= 64)) {
		// pca9548 address = 0x70..0x77
		ch_addr = (0x70 | ((ch - 1) / 8)); // calc pca9548 i2c address
		data = 0;
		if (on_off) {
			data = (1 << ((ch - 1) % 8)); // calc pca9548 channel
		}

		cmd = i2c_cmd_link_create();
	    i2c_master_start(cmd);
	    i2c_master_write_byte(cmd, (ch_addr << 1) | I2C_MASTER_WRITE, true); // ack_en = true
	    i2c_master_write(cmd, &data, 1, true); // ack_en = true
	    i2c_master_stop(cmd);
	    ret = i2c_master_cmd_begin(i2c_num, cmd, (I2C_CMD_TIMEOUT_MS / portTICK_RATE_MS));
	    i2c_cmd_link_delete(cmd);
	}
	else {
		ret = ESP_ERR_INVALID_ARG;
	}

	return ret;
}

esp_err_t I2CDev::detect(int ch, int addr) {
	i2c_cmd_handle_t cmd;
	esp_err_t ret;

	if (ch > 0) {
		ret = ch_sw(ch, true);
		if (ret != ESP_OK) {
			return ret;
		}
	}

	cmd = i2c_cmd_link_create();
	i2c_master_start(cmd);
	i2c_master_write_byte(cmd, (addr << 1) | I2C_MASTER_WRITE, true); // ack_en = true
	i2c_master_stop(cmd);
	ret = i2c_master_cmd_begin(i2c_num, cmd, (I2C_CMD_TIMEOUT_MS / portTICK_RATE_MS));
	i2c_cmd_link_delete(cmd);

	if (ch > 0) {
		ch_sw(ch, false);
	}

	// ESP_OK					Success
	// ESP_ERR_INVALID_ARG		Parameter error
	// ESP_FAIL					Sending command error, slave doesn’t ACK the transfer.
	// ESP_ERR_INVALID_STATE	I2C driver not installed or not in master mode.
	// ESP_ERR_TIMEOUT			Operation timeout because the bus is busy.
    return ret;
}

esp_err_t I2CDev::write(int ch, int addr, uint8_t *data, size_t data_size) {
	i2c_cmd_handle_t cmd;
	esp_err_t ret;

	if (ch == 0) {
		ret = ESP_OK;
	}
	else
	if ((ch >= 1) && (ch <= 64)) {
		ret = ch_sw(ch, true);
	}
	else {
		ret = ESP_ERR_INVALID_ARG;
	}

	if (ret == ESP_OK) {
		cmd = i2c_cmd_link_create();
		i2c_master_start(cmd);
		i2c_master_write_byte(cmd, (addr << 1) | I2C_MASTER_WRITE, true); // ack_en = true
		i2c_master_write(cmd, data, data_size, true); // ack_en = true
		i2c_master_stop(cmd);
		ret = i2c_master_cmd_begin(i2c_num, cmd, (I2C_CMD_TIMEOUT_MS / portTICK_RATE_MS));
		i2c_cmd_link_delete(cmd);

		if ((ch >= 1) && (ch <= 64)) {
			ch_sw(ch, false);
		}
	}

    return ret;
}

esp_err_t I2CDev::read(int ch, int addr, uint8_t *pointer, size_t pointer_size, uint8_t *data, size_t data_size) {
	i2c_cmd_handle_t cmd;
	esp_err_t ret;

	if (data_size == 0) {
        return ESP_OK;
    }

	if (ch == 0) {
		ret = ESP_OK;
	}
	else
	if ((ch >= 1) && (ch <= 64)) {
		ret = ch_sw(ch, true);
	}
	else {
		ret = ESP_ERR_INVALID_ARG;
	}

	if (ret == ESP_OK) {
		cmd = i2c_cmd_link_create();
		i2c_master_start(cmd);
		// write pointer
		if (pointer_size > 0) {
			i2c_master_write_byte(cmd, (addr << 1) | I2C_MASTER_WRITE, true); // ack_en = true
			i2c_master_write(cmd, pointer, pointer_size, true); // ack_en = true
			// repeat start
			i2c_master_start(cmd);
		}
		// read data
	    i2c_master_write_byte(cmd, (addr << 1) | I2C_MASTER_READ, true); // ack_en = true
	    if (data_size > 1) {
	        i2c_master_read(cmd, data, data_size - 1, I2C_MASTER_ACK);
	    }
	    i2c_master_read_byte(cmd, data + data_size - 1, I2C_MASTER_NACK);//I2C_MASTER_LAST_NACK);
	    i2c_master_stop(cmd);
	    ret = i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS);
	    i2c_cmd_link_delete(cmd);

		if ((ch >= 1) && (ch <= 64)) {
			ch_sw(ch, false);
		}
	}

    return ret;
}
