#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "i2c-dev.h"
#include "spi-dev.h"
#include "kidbright32.h"
#include "devman.h"
#include "mcp7940n.h"

//#define __DEBUG__

static I2CDev I2C0 = I2CDev(I2C_NUM_0, DISPLAY_SDA_GPIO, DISPLAY_SCL_GPIO, DISPLAY_I2CBUS_SPEED);
static I2CDev I2C1 = I2CDev(I2C_NUM_1, CHAIN_SDA_GPIO, CHAIN_SCL_GPIO, CHAIN_I2CBUS_SPEED);
static SPIDev SPI = SPIDev(SPIBUS_MISO_GPIO, SPIBUS_MOSI_GPIO, SPIBUS_CLK_GPIO, SPIBUS_CS_GPIO, SPIBUS_MAX_TRANSFER_SIZE);

extern MCP7940N mcp7940n;

// https://stackoverflow.com/questions/10811439/malloc-an-array-of-struct-pointers#answer-10811531
struct dev_list_struct {
	Driver *driver;
	Device **devices;
	int max_device, count;
	TickType_t task_idle_ms;
	enum {
		s_init, s_wait, s_proc
	} state;
} io_dev_list, i2c0_dev_list, i2c1_dev_list, spi_dev_list;

void device_list_init(dev_list_struct *dev_list, int max_device, TickType_t task_idle_ms) {
	int i;

	dev_list->devices = (Device **)malloc((max_device + 1) * sizeof(Device *));
	for (i = 0; i < (max_device + 1); i++) {
		dev_list->devices[i] = NULL;
	}

	dev_list->max_device = max_device;
	dev_list->count = 0;
	dev_list->state = dev_list->s_init;
	dev_list->task_idle_ms = task_idle_ms;
}

void vDevListTask(void *pvParameters) {
	dev_list_struct *dev_list = (dev_list_struct *)pvParameters;
	int i;
	bool ready;

	while (1) {
		switch (dev_list->state) {
			case dev_list->s_init:
				// devices init
				i = 0;
				while ((dev_list->devices[i] != NULL) && (i < (dev_list->max_device + 1))) {
					dev_list->devices[i]->init();
					i++;
				}

				// goto wait all devices ready
				dev_list->state = dev_list->s_wait;
				break;

			case dev_list->s_wait:
				// call device state machine for initialization
				ready = true;
				i = 0;
				while ((dev_list->devices[i] != NULL) && (i < (dev_list->max_device + 1))) {
					// device process handler
					dev_list->devices[i]->process(dev_list->driver);
					// check device initialized or error
					ready = ready && (dev_list->devices[i]->is_initialized() || dev_list->devices[i]->is_error());
					i++;
				}

				// wait all devices initialized or error
				if (ready) {
					// devices initialized or error
					dev_list->state = dev_list->s_proc;
				}
				break;

			case dev_list->s_proc:
				i = 0;
				while ((dev_list->devices[i] != NULL) && (i < (dev_list->max_device + 1))) {
					// device process handler
					dev_list->devices[i]->process(dev_list->driver);
					i++;
				}
				break;
		}

		// state machine delay
		vTaskDelay(dev_list->task_idle_ms / portTICK_RATE_MS);
	}
}

int devman_add(char *dev_name, driver_t drv, Device *dev) {
	int i;

	switch (drv) {
		case DEV_IO:
			// add to i/o device list
			for (i = 0; i < (io_dev_list.max_device + 1); i++) {
				if (io_dev_list.devices[i] == NULL) {
					// update device name
					snprintf(dev->name, DEVICE_NAME_LEN_MAX, "%s", dev_name);
					// set driver to null
					io_dev_list.driver = NULL;
					// add device to list
					io_dev_list.devices[i] = dev;
					// device count
					io_dev_list.count++;
					#ifdef __DEBUG__
						printf("I/O add device at index %d", i);
					#endif
					return i;
				}
			}
			break;

		case DEV_I2C0:
			// check i2c device address
			if (dev->address != -1) {
				// add to i2c0 device list
				for (i = 0; i < (i2c0_dev_list.max_device + 1); i++) {
					if (i2c0_dev_list.devices[i] == NULL) {
						// update device name
						snprintf(dev->name, DEVICE_NAME_LEN_MAX, "%s", dev_name);
						// set driver to i2c0
						i2c0_dev_list.driver = &I2C0;
						// add device to list
						i2c0_dev_list.devices[i] = dev;
						// device count
						i2c0_dev_list.count++;
						#ifdef __DEBUG__
							printf("I2C0 add device 0x%02x at index %d", dev->address, i);
						#endif
						return i;
					}
				}
			}
			break;

		case DEV_I2C1:
			// check i2c device address
			if (dev->address != -1) {
				// add to i2c1 device list
				for (i = 0; i < (i2c1_dev_list.max_device + 1); i++) {
					if (i2c1_dev_list.devices[i] == NULL) {
						// update device name
						snprintf(dev->name, DEVICE_NAME_LEN_MAX, "%s", dev_name);
						// set driver to i2c0
						i2c1_dev_list.driver = &I2C1;
						// add device to list
						i2c1_dev_list.devices[i] = dev;
						// device count
						i2c1_dev_list.count++;
						#ifdef __DEBUG__
							printf("I2C1 add device 0x%02x at index %d", dev->address, i);
						#endif
						return i;
					}
				}
			}
			break;

		case DEV_SPI:
			// check spi device address
			if (dev->address != -1) {
				// add to spi device list
				for (i = 0; i < (spi_dev_list.max_device + 1); i++) {
					if (spi_dev_list.devices[i] == NULL) {
						// update device name
						snprintf(dev->name, DEVICE_NAME_LEN_MAX, "%s", dev_name);
						// set driver to spi
						spi_dev_list.driver = &SPI;
						// add device to list
						spi_dev_list.devices[i] = dev;
						// device count
						spi_dev_list.count++;
						#ifdef __DEBUG__
							printf("SPI add device 0x%02x at index %d", dev->address, i);
						#endif
						return i;
					}
				}
			}
			break;
	}

	return -1;
}

int devman_count(driver_t drv) {
	switch (drv) {
		case DEV_IO:
			return io_dev_list.count;
			break;

		case DEV_I2C0:
			return i2c0_dev_list.count;
			break;

		case DEV_I2C1:
			return i2c1_dev_list.count;
			break;

		case DEV_SPI:
			return spi_dev_list.count;
			break;
	}

	return 0;
}

Device *devman_get(driver_t drv, int indx) {
	switch (drv) {
		case DEV_IO:
			return io_dev_list.devices[indx];
			break;

		case DEV_I2C0:
			return i2c0_dev_list.devices[indx];
			break;

		case DEV_I2C1:
			return i2c1_dev_list.devices[indx];
			break;

		case DEV_SPI:
			return spi_dev_list.devices[indx];
			break;
	}

	return NULL;
}

void devman_start(void) {
	// i/o device list task
	xTaskCreatePinnedToCore(vDevListTask, "I/O Device List Task", IO_DEV_STACK_SIZE_MIN, &io_dev_list, IO_DEV_TASK_PRIORITY, NULL, KIDBRIGHT_RUNNING_CORE);
	// i2c0 device list task
	xTaskCreatePinnedToCore(vDevListTask, "I2C0 Device List Task", I2C0_DEV_STACK_SIZE_MIN, &i2c0_dev_list, I2C0_DEV_TASK_PRIORITY, NULL, KIDBRIGHT_RUNNING_CORE);
	// i2c1 device list task
	xTaskCreatePinnedToCore(vDevListTask, "I2C1 Device List Task", I2C1_DEV_STACK_SIZE_MIN, &i2c1_dev_list, I2C1_DEV_TASK_PRIORITY, NULL, KIDBRIGHT_RUNNING_CORE);
	// spi device list task
	xTaskCreatePinnedToCore(vDevListTask, "SPI Device List Task", SPI_DEV_STACK_SIZE_MIN, &spi_dev_list, SPI_DEV_TASK_PRIORITY, NULL, KIDBRIGHT_RUNNING_CORE);
}

bool devman_ready(void) {
	// device manager ready, if all device count in device list is zero or all device state is in s_proc
	return (
		((io_dev_list.count == 0) || (io_dev_list.state == io_dev_list.s_proc)) &&
		((i2c0_dev_list.count == 0) || (i2c0_dev_list.state == i2c0_dev_list.s_proc)) &&
		((i2c1_dev_list.count == 0) || (i2c1_dev_list.state == i2c1_dev_list.s_proc)) &&
		((spi_dev_list.count == 0) || (spi_dev_list.state == spi_dev_list.s_proc))
	);
}

bool isdigit_str(char *str) {
	while (*str) {
		if (!isdigit(*str)) {
			return false;
		}
		str++;
	}

	return true;
}

bool devman_cli(char *line, char list[][MAX_UART_LINE_CHAR_COUNT], int len) {
	char buf[64], *ptr;
	driver_t drv;
	Device *dev;
	int prop_indx;
	uint8_t mac[6];

	// get
	if ((len == 2) && (strcmp(list[1], "get") == 0)) {
		if (strcmp(list[0], "wifi_mac") == 0) {
			// https://github.com/espressif/esp-idf/blob/master/components/esp32/system_api.c#L172
			// ESP_MAC_WIFI_STA, ESP_MAC_WIFI_SOFTAP, ESP_MAC_BT, ESP_MAC_ETH
			//
			// I (11104) system_api: Base MAC address is not set, read default base MAC address from BLK0 of EFUSE
			// I (11104) user_app: 80:7D:3A:C6:23:80
			if (esp_read_mac(mac, ESP_MAC_WIFI_STA) == ESP_OK) {
				printf("ok %02x:%02x:%02x:%02x:%02x:%02x\r\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
				// handled
				return true;
			}
		}
	}
	else
	// set
	if ((len == 3) && (strcmp(list[1], "set") == 0)) {
		// rtc set 18043001000000
		if (strcmp(list[0], "rtc") == 0) {
			if (!mcp7940n.is_error()) {
				// rtc set datetime
				mcp7940n.write(list[2]);
				printf("ok\r\n");
				// handled
				return true;
			}
		}
	}
	else
	// =========================================================================
	// cli examples
	// =========================================================================
	// > dev_i2c1 count
	// < ok 4
	// > dev_i2c1 0 name
	// < ok lm73
	// > dev_i2c1 0 prop_count
	// < ok 1
	// > dev_i2c1 0 prop_name 0
	// < ok temperature
	// > dev_i2c1 0 prop_attr 0
	// < ok ---N-r--
	// > dev_i2c1 0 prop_read 0
	// < ok 32.250000
	// > dev_i2c1 0 prop_unit 0
	// < ok °C
	if (((len >= 2) && (len <= 5)) && (strlen(list[0]) >= 6)) {
		// dev_io, dev_i2c0, dev_i2c1, dev_spi
		snprintf(buf, 5, "%s", list[0]);
		if (strcmp(buf, "dev_") == 0) {
			ptr = &list[0][4];
			if (strcmp(ptr, "io") == 0) {
				drv = DEV_IO;
			}
			else
			if (strcmp(ptr, "i2c0") == 0) {
				drv = DEV_I2C0;
			}
			else
			if (strcmp(ptr, "i2c1") == 0) {
				drv = DEV_I2C1;
			}
			else
			if (strcmp(ptr, "spi") == 0) {
				drv = DEV_SPI;
			}
			else {
				return false;
			}

			// count
			if ((len == 2) && (strcmp(list[1], "count") == 0)) {
				printf("ok %d\r\n", devman_count(drv));
				// handled
				return true;
			}

			// name/channel/address/prop_count
			if ((len == 3) && isdigit_str(list[1])) {
				if ((dev = devman_get(drv, atoi(list[1]))) != NULL) {
					if (strcmp(list[2], "name") == 0) {
						printf("ok %s\r\n", dev->name);
					}
					else
					if (strcmp(list[2], "channel") == 0) {
						printf("ok %d\r\n", dev->channel);
					}
					else
					if (strcmp(list[2], "address") == 0) {
						printf("ok %d\r\n", dev->address);
					}
					else
					if (strcmp(list[2], "status") == 0) {
						dev->get_stat_str(buf);
						printf("ok %s\r\n", buf);
					}
					else
					if (strcmp(list[2], "prop_count") == 0) {
						printf("ok %d\r\n", dev->prop_count());
					}
					else {
						return false;
					}

					// handled
					return true;
				}
			}

			// prop_name/prop_attr/prop_read
			if ((len == 4) && isdigit_str(list[1]) && isdigit_str(list[3])) {
				if ((dev = devman_get(drv, atoi(list[1]))) != NULL) {
					prop_indx = atoi(list[3]);

					// prop_name
					if (strcmp(list[2], "prop_name") == 0) {
						if (dev->prop_name(prop_indx, buf)) {
							printf("ok %s\r\n", buf);
							// handled
							return true;
						}
					}

					// prop_unit
					if (strcmp(list[2], "prop_unit") == 0) {
						if (dev->prop_unit(prop_indx, buf)) {
							printf("ok %s\r\n", buf);
							// handled
							return true;
						}
					}

					// prop_attr
					if (strcmp(list[2], "prop_attr") == 0) {
						if (dev->prop_attr(prop_indx, buf)) {
							printf("ok %s\r\n", buf);
							// handled
							return true;
						}
					}

					// prop_read
					if (strcmp(list[2], "prop_read") == 0) {
						if (dev->prop_read(prop_indx, buf)) {
							printf("ok %s\r\n", buf);
							// handled
							return true;
						}
					}
				}
			}
		}
	}

	// not handled
	return false;
}

void devman_init(void) {
	// io device list init
	device_list_init(&io_dev_list, MAX_IO_DEVICE, IO_TASK_IDLE_MS);

	// i2c0 driver init
	if (I2C0.init() != ESP_OK) {
		#ifdef __DEBUG__
			printf("I2C0 init error!");
		#endif
	}
	else {
		#ifdef __DEBUG__
			printf("I2C0 init ok.");
		#endif
		// i2c0 device list init
		device_list_init(&i2c0_dev_list, MAX_I2C0_DEVICE, I2C0_TASK_IDLE_MS);
	}

	// i2c1 driver init
	if (I2C1.init() != ESP_OK) {
		#ifdef __DEBUG__
			printf("I2C1 init error!");
		#endif
	}
	else {
		#ifdef __DEBUG__
			printf("I2C1 init ok.");
		#endif
		// i2c1 device list init
		device_list_init(&i2c1_dev_list, MAX_I2C1_DEVICE, I2C1_TASK_IDLE_MS);
	}

	// spi driver init
	if (SPI.init() != ESP_OK) {
		#ifdef __DEBUG__
			printf("SPI init error!");
		#endif
	}
	else {
		#ifdef __DEBUG__
			printf("SPI init ok.");
		#endif
		// spi device list init
		device_list_init(&spi_dev_list, MAX_SPI_DEVICE, SPI_TASK_IDLE_MS);
	}
}
