#include "string.h"
#include "device.h"

bool Device::is_error(void) {
	return error;
}

bool Device::is_initialized(void) {
	return initialized;
}

void Device::get_stat_str(char *stat) {
	// init status string
	strcpy(stat, "--------");

	// -/-/-/-/-/-/initialized/error
	if (initialized) {
		stat[7] = 'i';
	}
	if (error) {
		stat[6] = 'e';
	}
}

void Device::get_attr_str(char *attr, int flag) {
	// init attribute string
	strcpy(attr, "--------");

	// -/read/write/notify
	if (IS_FLAG_SET(flag, PROP_ATTR_NOTIFY_FLAG)) {
		attr[7] = 'n';
	}
	if (IS_FLAG_SET(flag, PROP_ATTR_WRITE_FLAG)) {
		attr[6] = 'w';
	}
	if (IS_FLAG_SET(flag, PROP_ATTR_READ_FLAG)) {
		attr[5] = 'r';
	}

	// -/-/string/number
	if (IS_FLAG_SET(flag, PROP_ATTR_TYPE_NUMBER_FLAG)) {
		attr[3] = 'N';
	}
	if (IS_FLAG_SET(flag, PROP_ATTR_TYPE_STRING_FLAG)) {
		attr[2] = 'S';
	}
}

uint32_t Device::get_polling_ms(void) {
	return polling_ms;
}

void Device::set_polling_ms(uint32_t ms) {
	polling_ms = ms;
}

TickType_t Device::get_tickcnt(void) {
	return xTaskGetTickCount();
}

bool Device::is_tickcnt_elapsed(TickType_t tickcnt, uint32_t tickcnt_ms) {
	TickType_t curr_tickcnt = xTaskGetTickCount();

	if ((curr_tickcnt - tickcnt) >= (tickcnt_ms / portTICK_RATE_MS)) {
		tickcnt = curr_tickcnt;
		return true;
	}

	return false;
}
