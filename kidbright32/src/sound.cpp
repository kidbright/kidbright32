#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "driver/mcpwm.h"
#include "soc/mcpwm_reg.h"
#include "soc/mcpwm_struct.h"
#include "kidbright32.h"
#include "sound.h"

// http://newt.phys.unsw.edu.au/jw/notes.html
const uint32_t music_notes_freq[] = {
	261, // 0 = C4
	277, // 1 = C#4
	293, // 2 = D4
	311, // 3 = Eb4
	329, // 4 = E4
	349, // 5 = F4
	369, // 6 = F#4
	391, // 7 = G4
	415, // 8 = G#4
	440, // 9 = A4
	466, // 10 = Bb4
	493, // 11 = B4
	523, // 12 = C5
	554, // 13 = C#5
	587, // 14 = D5
	622, // 15 = Eb5
	659, // 16 = E5
	698, // 17 = F5
	740, // 18 = F#5
	784, // 19 = G5
	831, // 20 = G#5
	880, // 21 = A5
	932, // 22 = Bb5
	988, // 23 = B5
	1046, // 24 = C6
	1109, // 25 = C#6
	1175, // 26 = D6
	1244, // 27 = Eb6
	1318, // 28 = E6
	1396, // 29 = F6
	1480, // 30 = F#6
	1568, // 31 = G6
	1661, // 32 = G#6
	1760, // 33 = A6
	1865, // 34 = Bb6
	1976, // 35 = B6
	2093 // 36 = C7
};

SOUND::SOUND() {
	// PWM0A output to SOUND_PWM_GPIO pin
	mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, SOUND_PWM_GPIO);
	pwm_config.frequency = 523; // C5
	pwm_config.cmpr_a = 0; // set duty cycle = 0%
	pwm_config.cmpr_b = 0; // set duty cycle = 0%
	pwm_config.counter_mode = MCPWM_UP_COUNTER;
	pwm_config.duty_mode = MCPWM_DUTY_MODE_0;
	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config); // update pwm
}

void SOUND::init(void) {
	bpm = 120;
	duty_cycle = SOUND_DUTY_CYCLE_PERCENT_MAX;
	// default volume = 50%
	set_volume(50);
	// clear error flag
	error = false;
	// set initialized flag
	initialized = true;
}

int SOUND::prop_count(void) {
	// not supported
	return 0;
}

bool SOUND::prop_name(int index, char *name) {
	// not supported
	return false;
}

bool SOUND::prop_unit(int index, char *unit) {
	// not supported
	return false;
}

bool SOUND::prop_attr(int index, char *attr) {
	// not supported
	return false;
}

bool SOUND::prop_read(int index, char *value) {
	// not supported
	return false;
}

bool SOUND::prop_write(int index, char *value) {
	// not supported
	return false;
}

void SOUND::process(Driver *drv) {
	//
}

void SOUND::on(uint32_t freq, uint8_t duty_in_percent) {
	if (duty_in_percent < 100) {
		pwm_config.frequency = freq;
		pwm_config.cmpr_a = duty_in_percent; // set duty cycle = 0%
		mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config); // update pwm
	}
}

void SOUND::off(void) {
	pwm_config.cmpr_a = 0; // set duty cycle = 0%
	mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config); // update pwm
}

void SOUND::note(uint8_t note) {
	if ((duty_cycle > 0) && (note < sizeof(music_notes_freq))) {
		// on note with current volume
		on(music_notes_freq[note], duty_cycle);
	}
}

void SOUND::rest(uint8_t duration) {
	uint32_t quarter_delay;
	uint32_t delay = 0;
	/*
	[120 bpm]
	whole = 2000 ms
	haft = 1000 ms
	quarter delay = 60*1000/120 = 500 ms
	eighth = 250 ms
	sixteenth = 125
	*/
	quarter_delay = (60 * 1000) / bpm;
	switch (duration) {
		case 0:
			delay = 4 * quarter_delay;
			break;

		case 1:
			delay = 2 * quarter_delay;
			break;

		case 2:
			delay = quarter_delay;
			break;

		case 3:
			delay = quarter_delay / 2;
			break;

		case 4:
			delay = quarter_delay / 4;
			break;

		default:
			delay = quarter_delay / 4;
			break;
	}

	if (delay > 0) {
		vTaskDelay(delay / portTICK_RATE_MS);
	}
}

uint32_t SOUND::get_bpm(void) {
	return bpm;
}

void SOUND::set_bpm(uint32_t val) {
	bpm = val;
}

uint8_t SOUND::get_volume(void) {
	return volume;
}

void SOUND::set_volume(uint32_t val) {
	if (val > 100) {
		val = 100;
	}
	volume = val;
	// calc duty cycle
	duty_cycle = (SOUND_DUTY_CYCLE_PERCENT_MAX * val) / 100;
}
