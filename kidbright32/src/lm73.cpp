#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "kidbright32.h"
#include "lm73.h"

// https://github.com/zkemble/LM73/blob/master/arduino/LM73/LM73.cpp

// lm73 registers
#define LM73_REG_TEMPERATURE		0x00
#define LM73_REG_CONFIG				0x01
#define LM73_REG_THI				0x02
#define LM73_REG_TLOW				0x03
#define LM73_REG_CTRLSTATUS			0x04
#define LM73_REG_ID					0x05

#define LM73_POWER_OFF				0xc0
#define LM73_POWER_ON				0x40

LM73::LM73(int bus_ch, int dev_addr) {
	channel = bus_ch;
	address = dev_addr;
	polling_ms = LM73_POLLING_MS;
}

void LM73::init(void) {
	temperature = 0;
	state = s_detect;
}

int LM73::prop_count(void) {
	return 1;
}

bool LM73::prop_name(int index, char *name) {
	if (index == 0) {
		snprintf(name, DEVICE_PROP_NAME_LEN_MAX, "%s", "temperature");
		return true;
	}

	// not supported
	return false;
}

bool LM73::prop_unit(int index, char *unit) {
	if (index == 0) {
		snprintf(unit, DEVICE_PROP_UNIT_LEN_MAX, "%sC", "\xc2\xb0");
		return true;
	}

	// not supported
	return false;
}

bool LM73::prop_attr(int index, char *attr) {
	if (index == 0) {
		get_attr_str(attr, PROP_ATTR_READ_FLAG | PROP_ATTR_TYPE_NUMBER_FLAG); // read only, number
		return true;
	}

	// not supported
	return false;
}

bool LM73::prop_read(int index, char *value) {
	if (index == 0) {
		snprintf(value, DEVICE_PROP_VALUE_LEN_MAX, "%f", temperature);
		return true;
	}

	// not supported
	return false;
}

bool LM73::prop_write(int index, char *value) {
	// not supported
	return false;
}

void LM73::process(Driver *drv) {
	I2CDev *i2c = (I2CDev *)drv;
	uint8_t data[2], pointer;
	uint16_t temp;

	switch (state) {
		case s_detect:
			// stamp polling tickcnt
			polling_tickcnt = get_tickcnt();
			// detect i2c device
			if (i2c->detect(channel, address) == ESP_OK) {
				// put lm73 to power on state
				data[0] = LM73_REG_CONFIG;
				data[1] = LM73_POWER_ON;
				if (i2c->write(channel, address, data, 2) == ESP_OK) {
					// clear error flag
					error = false;
					// get current tickcnt
					tickcnt = get_tickcnt();
					state = s_read;
				}
				else {
					state = s_error;
				}
			}
			else {
				state = s_error;
			}
			break;

		case s_read:
			// read delay for 40ms
			if (is_tickcnt_elapsed(tickcnt, 40)) {
				pointer = 0;
				if (i2c->read(channel, address, &pointer, 1, data, 2) == ESP_OK) {
					// set initialized flag
					initialized = true;
					// calc temperature
					temp = (data[0] << 3) | ((data[1] >> 5) & 0x07);
					temperature = (double)temp / 4;
					state = s_pwr_off;
				}
				else {
					state = s_error;
				}
			}
			break;

		case s_pwr_off:
			// put lm73 to power off staate
			data[0] = LM73_REG_CONFIG;
			data[1] = LM73_POWER_OFF;
			if (i2c->write(channel, address, data, 2) == ESP_OK) {
				// load polling tickcnt
				tickcnt = polling_tickcnt;
				// goto wait and retry with detect state
				state = s_wait;
			}
			else {
				state = s_error;
			}
			break;

		case s_error:
			// set error flag
			error = true;
			// clear initialized flag
			initialized = false;
			// get current tickcnt
			tickcnt = get_tickcnt();
			// goto wait and retry with detect state
			state = s_wait;
			break;

		case s_wait:
			// wait polling_ms timeout
			if (is_tickcnt_elapsed(tickcnt, polling_ms)) {
				state = s_detect;
			}
			break;
	}
}

double LM73::get(void) {
	return temperature;
}
