#include <stdio.h>
#include <string.h>
#include "esp_system.h"
#include "driver/adc.h"
#include "kidbright32.h"
#include "ldr.h"

// https://esp-idf.readthedocs.io/en/latest/api-reference/peripherals/adc.html#application-examples
// GPIO36(SENSOR_VP) = ADC1 CH0 input

#define MAX_LDR_VALUE						3400 // calibated value

LDR::LDR(void) {
	polling_ms = LDR_POLLING_MS;
	// config adc1
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_DB_0);
}

void LDR::init(void) {
	light = 0;
	// clear error flag
	error = false;
	state = s_read;
}

int LDR::prop_count(void) {
	return 1;
}

bool LDR::prop_name(int index, char *name) {
	if (index == 0) {
		snprintf(name, DEVICE_PROP_NAME_LEN_MAX, "%s", "light");
		return true;
	}

	// not supported
	return false;
}

bool LDR::prop_unit(int index, char *unit) {
	if (index == 0) {
		snprintf(unit, DEVICE_PROP_UNIT_LEN_MAX, "%s", "%");
		return true;
	}

	// not supported
	return false;
}

bool LDR::prop_attr(int index, char *attr) {
	if (index == 0) {
		get_attr_str(attr, PROP_ATTR_READ_FLAG | PROP_ATTR_TYPE_NUMBER_FLAG); // read only, number
		return true;
	}

	// not supported
	return false;
}

bool LDR::prop_read(int index, char *value) {
	if (index == 0) {
		snprintf(value, DEVICE_PROP_VALUE_LEN_MAX, "%d", light);
		return true;
	}

	// not supported
	return false;
}

bool LDR::prop_write(int index, char *value) {
	// not supported
	return false;
}

void LDR::process(Driver *drv) {
	int dat;

	switch (state) {
		case s_read:
			// stamp polling tickcnt
			polling_tickcnt = get_tickcnt();

			dat = adc1_get_raw(ADC1_CHANNEL_0);
			if (dat > MAX_LDR_VALUE) {
				dat = MAX_LDR_VALUE;
			}
			light = ((MAX_LDR_VALUE - dat) * 100) / MAX_LDR_VALUE;

			// set initialized flag
			initialized = true;
			state = s_wait;
			break;

		case s_wait:
			// wait polling_ms timeout
			if (is_tickcnt_elapsed(polling_tickcnt, polling_ms)) {
				state = s_read;
			}
			break;
	}
}

uint8_t LDR::get(void) {
	return light;
}
