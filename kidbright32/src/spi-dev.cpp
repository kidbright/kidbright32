#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "spi-dev.h"
#include "kidbright32.h"

// mcp23s17 registers
#define MCP23S17_REG_IODIRA				0x00
#define MCP23S17_REG_IODIRB				0x01
#define MCP23S17_REG_IOCON				0x0a
#define MCP23S17_REG_GPIOA				0x12
#define MCP23S17_REG_GPIOB				0x13

SPIDev::SPIDev(gpio_num_t _miso, gpio_num_t _mosi, gpio_num_t _clk, gpio_num_t _cs, int _max_transfer_sz) {
	miso = _miso;
	mosi = _mosi;
	clk = _clk;
	cs = _cs;
	max_transfer_sz = _max_transfer_sz;
}

esp_err_t SPIDev::init(void) {
	esp_err_t ret;
    spi_bus_config_t buscfg;
	spi_device_interface_config_t devcfg;
	spi_transaction_t trans;
	uint8_t buf[4];
	int i;

	// default spi low speed
	sck_speed = SCK_SPEED_LOW;

	/*
	cs
	|____ mcp23s17 @0x20 => reserved for 16-Output mcp23s17 spi chain board
	|____ mcp23s17 @0x21 => reserved for 16-Input mcp23s17 spi chain board
	|____ mcp23s17 @0x22 => reserved for 8-Output/8-Input mcp23s17 spi chain board
	|____ mcp23s17 @0x23 => reserved for 8-Output/8-Input mcp23s17 spi chain board
	|____ mcp23s17 @0x24 => cs 1..16
	|____ mcp23s17 @0x25 => cs 17..32
	|____ mcp23s17 @0x26 => cs 33..48
	|____ mcp23s17 @0x27 => cs 49..64
	*/
	memset(&buscfg, 0, sizeof(buscfg));
	buscfg.miso_io_num = miso;
	buscfg.mosi_io_num = mosi;
	buscfg.sclk_io_num = clk;
	buscfg.quadwp_io_num = -1;
	buscfg.quadhd_io_num = -1;
	buscfg.max_transfer_sz = max_transfer_sz;

	// initialize the spi bus
	// https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/peripherals/spi_master.html#spi-known-issues
    //if ((ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1)) == ESP_OK) { // full duplex mode, enable dma
	if ((ret = spi_bus_initialize(HSPI_HOST, &buscfg, 0)) == ESP_OK) { // half dupluex mode, disable dma
		memset(&devcfg, 0, sizeof(devcfg));
		devcfg.clock_speed_hz = SPIBUS_SPEED;
		devcfg.mode = SPIBUS_MODE;
		devcfg.spics_io_num = SPIBUS_CS_GPIO;
		devcfg.queue_size = SPIBUS_QUEUE_SIZE;
		devcfg.flags |= SPI_DEVICE_HALFDUPLEX;
		// attach device to spi bus
		if ((ret = spi_bus_add_device(HSPI_HOST, &devcfg, &spi)) == ESP_OK) {
			// low speed spi (1mhz)
			memset(&devcfg, 0, sizeof(devcfg));
			devcfg.clock_speed_hz = 1000000;
			devcfg.mode = SPIBUS_MODE;
			devcfg.spics_io_num = -1; // manual cs
			devcfg.queue_size = SPIBUS_QUEUE_SIZE;
			devcfg.flags |= SPI_DEVICE_HALFDUPLEX;
			if ((ret = spi_bus_add_device(HSPI_HOST, &devcfg, &ls_spi)) == ESP_OK) {
				// high speed spi (4mhz)
				memset(&devcfg, 0, sizeof(devcfg));
				devcfg.clock_speed_hz = 4000000;
				devcfg.mode = SPIBUS_MODE;
				devcfg.spics_io_num = -1; // manual cs
				devcfg.queue_size = SPIBUS_QUEUE_SIZE;
				devcfg.flags |= SPI_DEVICE_HALFDUPLEX;
				if ((ret = spi_bus_add_device(HSPI_HOST, &devcfg, &hs_spi)) == ESP_OK) {
					// enable all mcp23s17 hardware address
					memset(&trans, 0, sizeof(trans)); // zero out the transaction
					buf[0] = 0x40; // opcode write
					buf[1] = MCP23S17_REG_IOCON;
					buf[2] = 0x08; // enable HAEN bit
					buf[3] = 0x08; // enable HAEN bit
					trans.tx_buffer = buf;
					trans.length = 8 * sizeof(buf); // length in bits
					if ((ret = spi_device_transmit(spi, &trans)) == ESP_OK) {
						// init all spi cs to de-select state
						for (i = 4; i <= 7; i++) {
							memset(&trans, 0, sizeof(trans)); // zero out the transaction
							buf[0] = 0x40 | ((i << 1) & 0x0f); // opcode write
							buf[1] = MCP23S17_REG_GPIOA;
							buf[2] = 0xff; // de-select state
							buf[3] = 0xff; // de-select state
							trans.tx_buffer = buf;
							trans.length = 8 * sizeof(buf); // length in bits
							if ((ret = spi_device_transmit(spi, &trans)) != ESP_OK) {
								break;
							}
						}
						// init all spi cs direction to output
						for (i = 4; i <= 7; i++) {
							memset(&trans, 0, sizeof(trans)); // zero out the transaction
							buf[0] = 0x40 | ((i << 1) & 0x0f); // opcode write
							buf[1] = MCP23S17_REG_IODIRA;
							buf[2] = 0x00; // output
							buf[3] = 0x00; // output
							trans.tx_buffer = buf;
							trans.length = 8 * sizeof(buf); // length in bits
							if ((ret = spi_device_transmit(spi, &trans)) != ESP_OK) {
								break;
							}
						}
					}
				}
			}
		}
	}

	return ret;
}

void SPIDev::sck_speed_set(sck_speed_t _sck_speed) {
	sck_speed = _sck_speed;
}

esp_err_t SPIDev::ch_sw(int ch, bool on_off) {
	spi_transaction_t trans;
	uint16_t data16;
	uint8_t buf[3];
	uint8_t ch_addr;
	esp_err_t ret;

	if ((ch >= 1) && (ch <= 64)) {
		// mcp23s17 16-ch, address = 0x04-0x07 (ch1-ch64)
		ch_addr = (0x04 | ((ch - 1) / 16)); // calc mcp23s17 spi address
		data16 = 0;
		if (on_off) {
			data16 = (1 << ((ch - 1) % 16)); // calc mcp23s17 channel
		}
		// opcode write
		buf[0] = 0x40 | ((ch_addr << 1) & 0x0f);
		// register address
		if ((1 << ((ch - 1) % 16)) <= 0x0080) {
			// GPIOA register
			buf[1] = MCP23S17_REG_GPIOA;
			// cs active low
			buf[2] = ((uint8_t)data16 ^ 0xff);
		}
		else {
			// GPIOB register
			buf[1] = MCP23S17_REG_GPIOB;
			// cs active low
			buf[2] = ((uint8_t)(data16 >> 8) ^ 0xff);
		}

		memset(&trans, 0, sizeof(trans)); // zero out the transaction
		trans.tx_buffer = buf;
		trans.length = 8 * sizeof(buf);
		ret = spi_device_transmit(spi, &trans);
	}
	else {
		ret = ESP_ERR_INVALID_ARG;
	}

	return ret;
}

esp_err_t SPIDev::detect(int ch, int addr) {
	// detect not supported for spi device
    return ESP_OK;
}

esp_err_t SPIDev::write(int ch, int addr, uint8_t *data, size_t data_size) {
	spi_transaction_t trans;
	esp_err_t ret;
	spi_device_handle_t spi_handle = spi;

	if (ch == 0) {
		ret = ESP_OK;
	}
	else
	if ((ch >= 1) && (ch <= 64)) {
		// select spi handle
		if (sck_speed == SCK_SPEED_HIGH) {
			spi_handle = hs_spi;
		}
		else {
			spi_handle = ls_spi;
		}

		ret = ch_sw(ch, true);
	}
	else {
		ret = ESP_ERR_INVALID_ARG;
	}

	if (ret == ESP_OK) {
		memset(&trans, 0, sizeof(trans)); // zero out the transaction
		trans.tx_buffer = data;
		trans.length = (8 * data_size);
		ret = spi_device_transmit(spi_handle, &trans);

		if ((ch >= 1) && (ch <= 64)) {
			ch_sw(ch, false);
		}
	}

    return ret;
}

esp_err_t SPIDev::read(int ch, int addr, uint8_t *pointer, size_t pointer_size, uint8_t *data, size_t data_size) {
	spi_transaction_t trans;
	esp_err_t ret;
	spi_device_handle_t spi_handle = spi;

	if (ch == 0) {
		ret = ESP_OK;
	}
	else
	if ((ch >= 1) && (ch <= 64)) {
		// select spi handle
		if (sck_speed == SCK_SPEED_HIGH) {
			spi_handle = hs_spi;
		}
		else {
			spi_handle = ls_spi;
		}

		ret = ch_sw(ch, true);
	}
	else {
		ret = ESP_ERR_INVALID_ARG;
	}

	if (ret == ESP_OK) {
		memset(&trans, 0, sizeof(trans)); // zero out the transaction
		trans.tx_buffer = pointer;
		trans.length = (8 * pointer_size);
		trans.rx_buffer = data;
		trans.rxlength = (8 * data_size);
		ret = spi_device_transmit(spi_handle, &trans);

		if ((ch >= 1) && (ch <= 64)) {
			ch_sw(ch, false);
		}
	}

	return ret;
}
