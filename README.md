# kidbright32 static link library (libkidbright32.a)

### build (esp-idf release/v3.1)
PATH=$PATH:<TOOLCHAIN_BIN_PATH> IDF_PATH=<ESP_IDF_PATH> make component-kidbright32-build

### clean
PATH=$PATH:<TOOLCHAIN_BIN_PATH> IDF_PATH=<ESP_IDF_PATH> make component-kidbright32-clean
